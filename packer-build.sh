#!/usr/bin/env bash

set -e
PACKER=../../../product/hashicorp/packer/packer

if [ -e ~/.vagrant.d/boxes/virtualbox/Windows2012-R2 ]; then
    rm -rf ~/.vagrant.d/boxes/virtualbox/Windows2012-R2
    echo "boxes deleted"
fi

eval '${PACKER} build -only=virtualbox-iso windows_2012_r2.json'

