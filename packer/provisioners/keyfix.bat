@echo off
:: キーボード配列切替スクリプト

setlocal
:: グローバル変数定義エリア
set REG_PATH="HKLM\SYSTEM\CurrentControlSet\Services\i8042prt\Parameters"
set JPN=^/f^ ^/v^ ^"LayerDriver JPN^" ^/t^ REG_SZ ^/d
set OKI=^/f^ ^/v^ ^"OverrideKeyboardIdentifier^"^ ^/t REG_SZ^ ^/d
set OKS=^/f^ ^/v^ ^"OverrideKeyboardSubtype^"^ ^/t^ REG_DWORD^ ^/d
set OKT=^/f^ ^/v^ ^"OverrideKeyboardType^"^ ^/t^ REG_DWORD^ ^/d

:: メイン処理
  :: オプション判定処理 
  if "%1" EQU "/jis106" ( 
    call :setJPkeyboard
    call :end
  ) else if "%1" EQU "/us101" ( 
    call :setUSkeyboard
    call :end
  ) else ( 
    call :printUsage
    call :end
  ) 
  exit /b
:: 日本語配列
:setJPkeyboard
reg add %REG_PATH% %JPN% kbd106.dll
reg add %REG_PATH% %OKI% PCAT_106KEY
reg add %REG_PATH% %OKS% 2
reg add %REG_PATH% %OKT% 7
exit /b

:: 英語配列
:setUSkeyboard
reg add %REG_PATH% %JPN% kbd101.dll
reg add %REG_PATH% %OKI% PCAT_101KEY
reg add %REG_PATH% %OKS% 0
reg add %REG_PATH% %OKT% 7
exit /b

:: ヘルプ画面の表示
:printUsage
  echo Discriptions: Keyboard layout changes.
  echo %~n0: [/us101][/jis106]
  echo   /us101   ・・・US 配列
  echo   /jis106  ・・・JIS配列
exit /b

:: 終了処理
:end
endlocal
