@echo off
set BOXES=.\packer\vagrant-boxes\virtualbox\Windows2012-R2.box
dir %BOXES% >NUL 2>&1
If Errorlevel 0 del /a %BOXES% >NUL 2>&1
packer build -only=virtualbox-iso windows_2012_r2.json
